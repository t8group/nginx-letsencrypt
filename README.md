# t8group/nginx-letsencrypt

Simple nginx + letsencrypt (certbot) image with auto renew based on [nginx:alpine](https://hub.docker.com/_/nginx)

Source on gitlab.com: [https://gitlab.com/t8group/nginx-letsencrypt](https://gitlab.com/t8group/nginx-letsencrypt)

# Supported tags

* 1.0.0 , 1.0 , 1 , latest

# How to use this image

## Cerbot configuration

* Mount your [certbot.ini](https://eff-certbot.readthedocs.io/en/stable/using.html#configuration-file)
config to /etc/nginx-letsencrypt/certbot.ini:

```ini
# /etc/nginx-letsencrypt/certbot.ini file:
email = foo@example.com
agree-tos = true
```

* Or, just define your email for letsencrypt in CERTBOT_EMAIL environment variable:

```bash
docker run --env CERTBOT_EMAIL=foo@example.com ...
```

## List of required certificates

Mount config with a list of required certificates to /etc/nginx-letsencrypt/certificates.conf in format:

```
name1 certbot_args1
name2 certbot_args2
...
```

For example:

```
# /etc/nginx-letsencrypt/certificates.conf file:
example.com -d example.com,www.example.com
relay -d relay.example.com
```

## Accessing certificates

If you need to use certificates elsewhere you can mount a volume to /etc/letsencrypt, so certificates will be available at:

```
path_to_volume/live/name1/fullchain.pem
path_to_volume/live/name1/privkey.pem
...
```
For example:

```
path_to_volume/live/example.com/fullchain.pem
path_to_volume/live/example.com/privkey.pem
path_to_volume/live/relay/fullchain.pem
path_to_volume/live/relay/privkey.pem
```

## Nginx configuration

### Redirect http to https

All http requests are redirected to https with the same host and path by default

### Custom configs

Feel free to mount your configs to /etc/nginx/conf.d/ directory, for example:

```
# /etc/nginx/conf.d/my-site.conf file:
server {
    listen       443 ssl;
    server_name  example.com www.example.com;

    ssl_certificate /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/example.com/privkey.pem;
    include snippets/ssl-params.conf;
    include snippets/no-log-robots.conf;
    include snippets/no-log-favicon.conf;

    location / {
        proxy_set_header        Host $host;
        proxy_set_header        X-Real-IP $remote_addr;
        proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Proto $scheme;

        set $upstream        http://my-site;
        proxy_pass           $upstream;
    }
}
```

### Resolver

By default, the resolver is set to 127.0.0.11 but you can chanhge it globally by mounting /etc/nginx/conf.d/010-resolver.conf

```
# /etc/nginx/conf.d/010-resolver.conf file:
resolver 10.3.0.10 ipv6=off;
```

Or, set the resolver in your "server" sections.

### Predefined snippets

* snippets/ssl-params.conf

```
# Base params for ssl
ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
ssl_prefer_server_ciphers on;
ssl_ciphers "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH";
ssl_ecdh_curve secp384r1;
ssl_session_cache shared:SSL:10m;
ssl_session_tickets off;
```

* snippets/ssl-self-signed.conf

```
# Use self-signed certificate included in image
ssl_certificate /etc/nginx/ssl/self-signed.crt;
ssl_certificate_key /etc/nginx/ssl/self-signed.key;
```

* snippets/no-log-robots.conf

```
# Disable logs for /robots.txt requests
location = /robots.txt {
    access_log off;
    log_not_found off;
}
```

* snippets/no-log-favicon.conf

```
# Disable logs for /favicon.txt requests
location = /favicon.ico {
    access_log off;
    log_not_found off;
}
```

* snippets/no-log-2xx.conf

```
# Disable logs for sucessfully 2xx requests
# $not2xx variable defined in /etc/nginx/conf.d/020-vars.conf
access_log  /var/log/nginx/access.log  main if=$not2xx;
```
