ARG NGINX_VERSION=1.21-alpine
FROM nginx:$NGINX_VERSION

RUN apk add --no-cache bash certbot

COPY docker-entrypoint.d/ /docker-entrypoint.d/
COPY etc/ /etc/

RUN mkdir -p /var/local/letsencrypt/webroot/.well-known
